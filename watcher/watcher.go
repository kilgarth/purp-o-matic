package watcher

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"purp-o-matic/evedata"
	"time"

	log "github.com/sirupsen/logrus"
)

type Item struct {
	TypeName   string  `json:"type_name"`
	TypeID     int     `json:"type_id"`
	RegionName string  `json:"region_name"`
	RegionID   int     `json:"region_id"`
	OrderType  string  `json:"order_type"`
	Threshold  float64 `json:"threshold"`
}

type DiscordMsg struct {
	Content string `json:"content"`
	Text    string `json:"text"`
}

func (i *Item) StartWatcher(webhook string, interval int) {
	if i.TypeName == "" && i.TypeID == 0 {
		log.Errorf("Failed to start watcher, no valid item information given.")
		return
	}
	// Convert type name to id if not set
	if i.TypeID == 0 {
		r := evedata.ResolveName(i.TypeName, "item")
		i.TypeID = r.ID
		i.TypeName = r.Name
	}

	if i.TypeID != 0 {
		r := evedata.ResolveID(i.TypeID)
		i.TypeName = r.Name
	}

	if i.RegionName == "" && i.RegionID == 0 {
		log.Infof("No region information given for %s, using The Forge as default.", i.TypeName)
		i.RegionName = "The Forge"
	}

	// Convert region to id if not set
	if i.RegionID == 0 {
		r := evedata.ResolveName(i.RegionName, "region")
		i.RegionID = r.ID
		i.RegionName = r.Name
	}

	log.Infof("Starting watcher for %s in %s with threshold %.2f", i.TypeName, i.RegionName, i.Threshold)

	ticker := time.NewTicker(time.Duration(interval) * time.Minute)
	for _ = range ticker.C {
		log.Infof("Checking %s in %s...", i.TypeName, i.RegionName)
		price, system, found := evedata.GetPrice(i.TypeID, i.OrderType, i.RegionID)
		if !found {
			log.Infof("No orders found for %s in %s.", i.TypeName, i.RegionName)
			continue
		}
		if i.OrderType == "sell" {
			if price < i.Threshold {
				// Found order less than threshold, alert!
				log.Infof("Alert triggered for %s (%s) in %s!", i.TypeName, i.OrderType, i.RegionName)
				go alert(webhook, fmt.Sprintf("PURP ALERT! Sell order for **%s** found in **%s** for **$%s** which is less than the threshold of **$%s** set!", i.TypeName, system, iskFormat(price), iskFormat(i.Threshold)))
			}
		} else {
			if price > i.Threshold {
				// Found buy order for more than threshold, alert!
				log.Infof("Alert triggered for %s (%s) in %s!", i.TypeName, i.OrderType, i.RegionName)
				go alert(webhook, fmt.Sprintf("PURP ALERT! Buy order for **%s** found in **%s** for **$%s** which is higher than the threshold of **$%s** set!", i.TypeName, system, iskFormat(price), iskFormat(i.Threshold)))
			}
		}
		log.Infof("%s in %s has been processed successfully.", i.TypeName, i.RegionName)
	}
}

func alert(webhook string, message string) {
	payload := DiscordMsg{Text: message, Content: message}
	data, err := json.Marshal(payload)
	if err != nil {
		log.Errorf("Failed to marshal json for payload to discord: %s", err)
		return
	}
	req, _ := http.NewRequest("POST", webhook, bytes.NewBuffer(data))
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{
		Timeout: time.Second * 5,
	}
	res, err := client.Do(req)
	if err != nil {
		log.Errorf("Failed to send message to webhook: %s", err)
	}

	defer res.Body.Close()

	return
}

func iskFormat(amount float64) string {
	inStr := fmt.Sprintf("%.2f", amount)
	var outBuf []byte

	commaPos := -1 // -1 until we get past the decimal.
	for i := len(inStr) - 1; i >= 0; i-- {
		if commaPos == -1 {
			if inStr[i] == '.' {
				commaPos = 0
			}
		} else {
			commaPos++
		}

		outBuf = append(outBuf, inStr[i])
		if commaPos == 3 && i != 0 {
			commaPos = 0
			outBuf = append(outBuf, ',')
		}
	}

	for b, e := 0, len(outBuf)-1; b < e; b, e = b+1, e-1 {
		outBuf[b], outBuf[e] = outBuf[e], outBuf[b]
	}
	return string(outBuf)
}
