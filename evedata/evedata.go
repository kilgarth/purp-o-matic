package evedata

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

// Order data from api
type Order struct {
	Duration     int       `json:"duration"`
	IsBuyOrder   bool      `json:"is_buy_order"`
	Issued       time.Time `json:"issued"`
	LocationID   int       `json:"location_id"`
	MinVolume    int       `json:"min_volume"`
	Price        float64   `json:"price"`
	Range        string    `json:"range"`
	SystemID     int       `json:"system_id"`
	VolumeRemain int       `json:"volume_remain"`
	VolumeTotal  int       `json:"volume_total"`
}

// System data from api
type System struct {
	Name string `json:"name"`
}

type Result struct {
	Name string `json:"name"`
	ID   int    `json:"id"`
}

type LookupResult struct {
	Regions        []Result `json:"regions"`
	InventoryTypes []Result `json:"inventory_types"`
}

type IDResult struct {
	Category string `json:"category"`
	ID       int    `json:"id"`
	Name     string `json:"name"`
}

type IDLookup struct {
	IDs []int `json:"ids"`
}

// Get the price data and return just the highest/lowest and the system
func GetPrice(typeID int, orderType string, region int) (float64, string, bool) {
	var price float64
	var systemID int
	var systemName string
	var found bool

	page := 1
	for {
		req, _ := http.NewRequest("GET", fmt.Sprintf("https://esi.evetech.net/v1/markets/%d/orders/?order_type=%s&page=%d&type_id=%d", region, orderType, page, typeID), nil)
		client := &http.Client{
			Timeout: time.Second * 5,
		}

		res, err := client.Do(req)
		if err != nil {
			log.Errorf("Failed to process request for %d in %d: %s", typeID, region, err)
			return price, systemName, found
		}

		defer res.Body.Close()
		if res.StatusCode == 200 {
			var out []Order
			d := json.NewDecoder(res.Body)
			err = d.Decode(&out)
			if err != nil {
				log.Errorf("Failed to decode json output from ESI: %s", err)
				continue
			}
			if len(out) > 0 {
				found = true
				for _, o := range out {
					if orderType == "sell" {
						// Looking for sell orders, so we want lowest sell
						if price == 0 {
							price = o.Price
							systemID = o.SystemID
						} else if price > o.Price {
							// `price` is greater than this order price, we want the lowest price possible so set new lowest.
							price = o.Price
							systemID = o.SystemID
						}
					} else {
						// Looking for buy orders, so we want highest buy
						if price == 0 {
							price = o.Price
							systemID = o.SystemID
						} else if price < o.Price {
							price = o.Price
							systemID = o.SystemID
						}
					}
				}
			} else {
				break
			}
		}
		p := res.Header.Get("x-pages")
		if p != "" {
			pages, _ := strconv.Atoi(p)
			if page < pages {
				page++
			} else {
				break
			}
		}
	}

	if found {
		systemName = getSystem(systemID)
	}
	return price, systemName, found

}

func getSystem(systemID int) string {
	req, _ := http.NewRequest("GET", fmt.Sprintf("https://esi.evetech.net/v4/universe/systems/%d", systemID), nil)
	req.Header.Add("Accept", "application/json")
	client := &http.Client{
		Timeout: time.Second * 5,
	}
	res, err := client.Do(req)
	if err != nil {
		log.Errorf("Failed to resolve system ID %d: %s", systemID, err)
		return "Unknown"
	}
	defer res.Body.Close()

	b, _ := ioutil.ReadAll(res.Body)

	var sys System
	d := json.NewDecoder(bytes.NewBuffer(b))
	err = d.Decode(&sys)
	if err != nil {
		log.Errorf("Failed to decode output from ESI: %s", err)
		return "Unknown"
	}

	return sys.Name
}

func ResolveName(name string, lookupType string) *Result {
	var result Result
	body, err := json.Marshal([]string{name})
	if err != nil {
		log.Errorf("Failed to marshal name %s: %s", name, err)
		return &result
	}
	req, _ := http.NewRequest("POST", "https://esi.evetech.net/v1/universe/ids/?datasource=tranquility&language=en-us", bytes.NewBuffer(body))
	req.Header.Add("Accept", "application/json")
	// technically we don't need this header since we aren't sending data, but ccp is stupid and expects it....
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{
		Timeout: time.Second * 5,
	}

	res, err := client.Do(req)
	if err != nil {
		log.Errorf("Failed to resolve name %s: %s", name, err)
		return &result
	}

	defer res.Body.Close()

	if res.StatusCode == 200 {
		var out LookupResult
		d := json.NewDecoder(res.Body)
		err = d.Decode(&out)
		if err != nil {
			log.Errorf("Failed to unmarshal output from name resolution: %s", err)
			return &result
		}

		switch lookupType {
		case "region":
			if len(out.Regions) > 0 {
				result = out.Regions[0]
			}
		case "item":
			if len(out.InventoryTypes) > 0 {
				result = out.InventoryTypes[0]
			}
		}

	}
	return &result
}

func ResolveID(id int) *IDResult {
	var result IDResult
	var lookupRequest IDLookup
	lookupRequest.IDs = []int{id}
	body, err := json.Marshal(lookupRequest)
	if err != nil {
		log.Errorf("Failed to marshal id %d: %s", id, err)
		return &result
	}
	req, _ := http.NewRequest("POST", "https://esi.evetech.net/v1/universe/names/?datasource=tranquility&language=en-us", bytes.NewBuffer(body))
	req.Header.Add("Accept", "application/json")
	// technically we don't need this header since we aren't sending data, but ccp is stupid and expects it....
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{
		Timeout: time.Second * 5,
	}

	res, err := client.Do(req)
	if err != nil {
		log.Errorf("Failed to resolve id %d: %s", id, err)
		return &result
	}

	defer res.Body.Close()
	b, _ := ioutil.ReadAll(res.Body)

	if res.StatusCode == 200 {
		var out []IDResult
		d := json.NewDecoder(bytes.NewBuffer(b))
		err = d.Decode(&out)
		if err != nil {
			log.Errorf("Failed to unmarshal output from ID resolution: %s", err)
			return &result
		}

		if len(out) > 0 {
			result = out[0]
		}

	} else {
		log.Errorf("Got non-200 from API: %d\n%s", res.StatusCode, string(b))
	}
	return &result
}
