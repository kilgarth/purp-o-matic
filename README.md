# Purp-o-Matic
This is designed to query ESI for prices of items in a given region and notify you when something is found beyond a threshold set.

## Building

Run `dep ensure` to pull dependencies, then build the package.

## Config
See the `example-config.json` for the example config. Modify and save as `config.json` for use.

Options:

`check_interval`: How often to check the market data (in minutes).
`alert_webhook`: Discord/Slack webhook to use to send alerts.
`items`: Array of items to watch

### Item Config
**You must provide one of these options, either type ID or name.**

`type_name`: The exact type name of the item you want to watch.

`type_id`: The type ID of the item you want to watch.

**You must provide one of these options, either region ID or name.**

`region_id`: The region to look for orders in.

`region_name`: The name of the region to look for orders in.

`order_type`: Either `sell` or `buy`.

`threshold`: Value to use as threshold for notifications.

