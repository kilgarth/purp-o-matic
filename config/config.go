package config

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"purp-o-matic/watcher"

	log "github.com/sirupsen/logrus"
)

type Config struct {
	// Initialized will be set if the configuration has been loaded
	CheckInterval int            `json:"check_interval"`
	Items         []watcher.Item `json:"items"`
	AlertWebHook  string         `json:"alert_webhook"`
	Initialized   bool
}

var cfgFile string
var cfg *Config

func Init(file string) {
	// Init the vars
	cfgFile = file
	cfg = &Config{}
}

func Get() *Config {
	// Check to see if the config was already loaded and load if not
	if !cfg.Initialized {
		cfg = load()
	}

	return cfg
}

func load() *Config {
	var cfg Config
	file, err := ioutil.ReadFile(cfgFile)
	if err != nil {
		log.Error(err)
		return &cfg
	}

	d := json.NewDecoder(bytes.NewBuffer(file))
	err = d.Decode(&cfg)
	if err != nil {
		log.Error(err)
		return &cfg
	}

	cfg.Initialized = true

	return &cfg
}
