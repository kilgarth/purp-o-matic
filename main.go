package main

import (
	"flag"
	"os"
	"purp-o-matic/config"
	"purp-o-matic/watcher"
	"sync"

	log "github.com/sirupsen/logrus"
)

var cfgFile = flag.String("-c", "config.json", "Config file to load")
var logLevelString = flag.String("-l", "debug", "Log Level")

func main() {
	flag.Parse()
	logLevel, _ := log.ParseLevel(*logLevelString)
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(logLevel)

	log.Infof("Loading configuration from %s", *cfgFile)
	config.Init(*cfgFile)
	cfg := config.Get()

	wg := sync.WaitGroup{}
	wg.Add(len(cfg.Items))
	for _, w := range cfg.Items {
		go func(w watcher.Item) {
			w.StartWatcher(cfg.AlertWebHook, cfg.CheckInterval)
			log.Infof("Watcher for %s in %s has stopped.", w.TypeName, w.RegionName)
			wg.Done()
		}(w)
	}

	wg.Wait()
	log.Info("All watchers have stopped, exiting.")

}
